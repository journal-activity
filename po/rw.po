# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-06-19 00:30-0400\n"
"PO-Revision-Date: 2008-07-31 12:00+0200\n"
"Last-Translator: GASHAYIJA Guillaume <gashayija2002@yahoo.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Pootle 1.1.0rc2\n"

#: activity/activity.info:2
#: journalactivity.py:117
#: volumesmanager.py:58
msgid "Journal"
msgstr "Ikinyamakuru"

#: collapsedentry.py:225
#: expandedentry.py:180
#: palettes.py:54
msgid "Untitled"
msgstr "Nta mutwe w`amagambo"

#: detailview.py:121
msgid "Back"
msgstr "Inyuma"

#: expandedentry.py:225
msgid "No preview"
msgstr "Nta kibanziriza"

#: expandedentry.py:242
msgid "Participants:"
msgstr "Abagize uruhare"

#: expandedentry.py:267
msgid "Description:"
msgstr "Igisobanuro"

#: expandedentry.py:292
msgid "Tags:"
msgstr "Tags:"

#: journaltoolbox.py:51
msgid "Search"
msgstr "Shaka"

#: journaltoolbox.py:118
msgid "Anytime"
msgstr "Igihe icyo aricyo cyose"

#: journaltoolbox.py:120
msgid "Today"
msgstr "uyu munsi"

#: journaltoolbox.py:122
msgid "Since yesterday"
msgstr "kuva ejo hashize"

# TRANS: Filter entries modified during the last 7 days.
#. TRANS: Filter entries modified during the last 7 days.
#: journaltoolbox.py:124
msgid "Past week"
msgstr "icyumweru gishize"

# TRANS: Filter entries modified during the last 30 days.
#. TRANS: Filter entries modified during the last 30 days.
#: journaltoolbox.py:126
msgid "Past month"
msgstr "ukwezi gushize"

# TRANS: Filter entries modified during the last 356 days.
#. TRANS: Filter entries modified during the last 356 days.
#: journaltoolbox.py:128
msgid "Past year"
msgstr "umwaka ushize"

#: journaltoolbox.py:135
msgid "Anyone"
msgstr "Uwo ariwe wese"

#: journaltoolbox.py:137
msgid "My friends"
msgstr "Inshuti zanjye"

#: journaltoolbox.py:138
msgid "My class"
msgstr "Ishuri ryanjye"

# TRANS: Item in a combo box that filters by entry type.
#. TRANS: Item in a combo box that filters by entry type.
#: journaltoolbox.py:250
msgid "Anything"
msgstr "Icyo aricyo cyose"

#: journaltoolbox.py:316
#: palettes.py:70
msgid "Copy"
msgstr "Koporora"

#: journaltoolbox.py:326
#: palettes.py:78
msgid "Erase"
msgstr "Siba"

#. TRANS: Action label for resuming an activity.
#: journaltoolbox.py:390
#: palettes.py:60
msgid "Resume"
msgstr "Subira"

#. TRANS: Action label for starting an entry.
#: journaltoolbox.py:393
#: palettes.py:62
msgid "Start"
msgstr "Tangira"

#: listview.py:39
msgid "Your Journal is empty"
msgstr "Ikinyamakuru cyawe ntakintu cyanditsemo"

#: listview.py:40
msgid "No matching entries "
msgstr "Nta bihuje bibonetse"

#: misc.py:93
msgid "No date"
msgstr "Nta tariki"

#: objectchooser.py:130
msgid "Choose an object"
msgstr "Hitamo ikintu"

#: objectchooser.py:135
msgid "Close"
msgstr "Funga"

#: volumestoolbar.py:93
msgid "Unmount"
msgstr "Ibintu bike"

#~ msgid "Go back"
#~ msgstr "Subira inyuma"

